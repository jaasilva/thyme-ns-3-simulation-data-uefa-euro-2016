# README #

Trace files were generated with all the operations to be issued.
For that, we crawled tweets issued during the 2016 UEFA European Championship final, between Portugal and France.

Tweets were used as data objects, where:

* the tweet id was used as the object identifier;
* the text was used as the object data;
* the timestamp was used as the object insertion time; and
* the hashtags were used as the object tags.

The top-k most active users were chosen, and every other operation was generated from that, using exponential distributions configured with different lambda values (i.e., rates).

Subscriptions were generated taking into account the tags of the inserted objects, and the top 60% of the most popular tags were used for the subscriptions' queries (for simplicity sake, each subscription subscribed to one tag chosen at random).
Subscriptions were generated in two forms:

* time independent (ts^s = ts^e = NULL);
* and in the future (ts^s = NOW and ts^e = NULL$).

Time independent subscriptions where generated with a probability of 60%.
During the first half of the game, subscriptions were generated with a rate of three operations per user per hour, and reduced to one per user per hour for the remainder of the event.

Delete and unsubscribe operations, which are expected to be rare, were generated with a rate of 0.5 and 0.2 operations per user per hour, respectively, only during the second half of the game.

We crawled a total of three hours, starting at 20:00 2016-07-10.
To make the simulation execution more lively (and to reduce the simulation total time), we compressed the three hours into ten minutes of simulated time.
Since we use real tweets for trace generation, the distribution of operations in a trace file is irregular, with occasional spikes and void moments.

In the trace files, each operation has the following syntax (using '$|$' as the token separator):

#### Node Enter
Node shows up on the system for the first time

`NODE$|$<time>$|$<nodeId>`

#### Node Exit
Node exits the system permanently (for permanent/crash churn)

`EXIT$|$<time>$|$<nodeId>`

#### Node Pause
Node exits the system momentarily (for intermittent/momentary failure churn)

`PAUSE$|$<time>$|$<nodeId>`

#### Node Resume
Node reenters the system (for intermittent/momentary failure churn)

`RESUME$|$<time>$|$<nodeId>`

#### Publish Data
Publish a data object in the system

`PUB$|$<time>$|$<nodeId>$|$<objKey>$|$<obj>$|$<description>$|$<tags>`

#### Unpublish Data
Remove a data object from the system

`UNPUB$|$<time>$|$<nodeId>$|$<objKey>`

#### Subscribe
Subscribe to a tag in the past (start=NULL, end=NULL) or in the future (start=NOW, end=NULL)

`SUB_P$|$<time>$|$<nodeId>$|$<filter>`

`SUB_F$|$<time>$|$<nodeId>$|$<filter>`

#### Unsubscribe
Unsubscribe to a tag

`UNSUB$|$<time>$|$<nodeId>$|$<subId>`

#### Node Position
Set a node position

`$node_(<nodeId>) set X_ <position>
$node_(<nodeId>) set Y_ <position>`

#### Node Movement
Set a node movement

`$ns_ at <time> "$node_(<nodeId>) setdest <position_X> <position_Y> <speed>"`

#### Parameters Type

`<time>` is a double

`<nodeId>` is an integer

`<objKey>` is a string

`<obj>` is a string

`<tags>` is a string (with each tag separated by a blank space)

`<subId>` is an integer

`<position>` is a double

`<position_X>` is a double

`<position_Y>` is a double

`<speed>` is a double (in meters per second)

#### File Name Parsing
`n<nodes>-euro2016-t1-p<pause>-s<speed>-c<crash>-i<intermittent>.txt`

`<nodes>` specifies the number of nodes in the simulation

`<pause>` specifies the duration of pause moments during the simulation

`<speed>` specifies the average speed of nodes movement in the movement moments during the simulation

`<crash>` specifies the percentage of nodes that crash permanently

`<intermittent>` specifies the percantage of nodes that have a temporary crash
